'use strict';

$(document).ready(function () {

  /* $('a[href^="#"]').on('click', function (e) {
    e.preventDefault();
    var target = this.hash,
      $target = $(target);
     $('html, body').stop().animate({
      'scrollTop': $target.offset().top
    }, 900, 'swing', function () {
      window.location.hash = target;
    });
  }); */

  $('#bgndVideo').YTPlayer();

  $('.a-companies__cards').slick({
    slidesToShow: 6,
    slidesToScroll: 1,
    dots: false,
    arrows: true,
    centerMode: false,
    autoplay: true,
    responsive: [{
      breakpoint: 1199,
      settings: {
        slidesToShow: 1
      }
    }]
  });

  $('.a-reviews__slick').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: false,
    arrows: true,
    centerMode: false,
    autoplay: true
  });

  $('.phone').inputmask('+7(999)999-99-99');

  $('.h-header__menu').on('click', function (e) {
    e.preventDefault();
    $('.h-header__nav').slideToggle('fast', function (e) {
      // callback
    });
  });

  $('.a-advices__card-header').on('click', function (e) {
    e.preventDefault();
    $(this).children().next().toggleClass('a-advices__card-icon_closed');
    $(this).next().slideToggle('fast', function (e) {
      // callback
    });
  });

  $('.phone').inputmask('+7(999)999-99-99');

  $('.a-stepper__toggler').on('click', function (e) {
    e.preventDefault();

    // Remove active class from buttons
    $('.a-stepper__toggler').removeClass('active');

    // Add the correct active class
    $(this).addClass('active');

    // Hide all tab items
    $('[class^=tab-item]').hide();

    // Show the clicked tab
    var num = $(this).data('tab-item');
    $('.tab-item-' + num).fadeIn();
  });

  $('.a-steps__button').on('click', function (e) {
    e.preventDefault();

    $('.a-steps__combo').slideToggle('fast', function (e) {
      // callback
    });
    $('.a-stepper__wrapper').slideToggle('fast', function (e) {
      // callback
    });

    $('html, body').animate({
      scrollTop: $('#steps').offset().top
    }, 500);
  });

  $('.a-rate__blur').on('click', function (e) {
    e.preventDefault();

    $('.a-steps__combo').slideToggle('fast', function (e) {
      // callback
    });
    $('.a-stepper__wrapper').slideToggle('fast', function (e) {
      // callback
    });

    $('html, body').animate({
      scrollTop: $('#steps').offset().top
    }, 500);
  });

  $('.a-choose__card_first').on('click', function (e) {
    e.preventDefault();

    $('.a-steps__combo').slideToggle('fast', function (e) {
      // callback
    });
    $('.a-stepper__wrapper').slideToggle('fast', function (e) {
      // callback
    });
  });

  $('#a-stepper__close').on('click', function (e) {
    e.preventDefault();

    $('.a-steps__combo').slideToggle('fast', function (e) {
      // callback
    });
    $('.a-stepper__wrapper').slideToggle('fast', function (e) {
      // callback
    });
  });

  new TypeIt('#typing', {
    strings: 'за 3 шага на любой объект по оптовым ценам.',
    speed: 50,
    autoStart: false,
    loop: true,
    loopDelay: 5000
  });

  $('#main-form').submit(function (e) {
    e.preventDefault();
    var f = $(this);
    $('.ierror', f).removeClass('ierror');
    var person = $('input[name="person"]', f).val();
    var phone = $('input[name="phone"]', f).val();
    var comment = $('input[name="comment"]', f).val();
    var error = false;
    if (error) {
      return false;
    }
    $.ajax({
      type: 'POST',
      url: 'mail.php',
      data: $(this).serialize()
    }).done(function (data) {
      if (data.response == 'ok') {
        alert('Спасибо, ваша заявка отправлена!');
      } else {
        alert('Ошибка, попробуйте повторить позже!');
      }
    });
    return false;
  });
});